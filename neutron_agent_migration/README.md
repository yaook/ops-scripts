# Migration script

## Description

This script moves L3 HA routers or DHCP instances between agents using the Openstack API.

## Get an overview

To get all available L3 agents and the number of routers:

``` shell
python scheduler.py --agent l3 count-resources-per-agent
```

Output:

``` shell
ID                                    Agent                                           Availability Zone      Resources
------------------------------------  ----------------------------------------------  -------------------  -----------
4ffc358d-3ea0-41ce-9519-8766b4e23b84  gtw01                                           eu01-1                       210
ba7c8df7-2cd6-4cfd-85d4-927a7cfcdfa6  gtw02.os01.stage.eu01-m.iaas.int.stackit.cloud  eu01-2                         0
72b506c3-cf19-4276-9943-bb642d0d259f  gtw04.os01.stage.eu01-m.iaas.int.stackit.cloud  eu01-3                       154
4aa27313-659e-4fdf-a5bc-b7429ba1ad85  gtw05                                           eu01-1                       208
```

`Resources` in that case are routers, if you have set `dhcp` as agent these are networks.

## Drain agent

For example, to empty `gtw02.os01.stage.eu01-m.iaas.int.stackit.cloud` and `gtw04.os01.stage.eu01-m.iaas.int.stackit.cloud` and move them to all available instances you can call that:

``` shell
python scheduler.py --agent l3 drain --agents-to-drain ba7c8df7-2cd6-4cfd-85d4-927a7cfcdfa6 72b506c3-cf19-4276-9943-bb642d0d259f --max-resources-per-agent 600 --verify-active
```

You can also exclude other agents e.g. `gtw01` by calling it like this:

``` shell
python scheduler.py --agent l3 drain --agents-to-drain ba7c8df7-2cd6-4cfd-85d4-927a7cfcdfa6 72b506c3-cf19-4276-9943-bb642d0d259f --excluded-agents 4ffc358d-3ea0-41ce-9519-8766b4e23b84 --max-resources-per-agent 600 --verify-active
```

For more explanations of the arguments call the help for `drain`:

``` shell
python scheduler.py drain -h
```
