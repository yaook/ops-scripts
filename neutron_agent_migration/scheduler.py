"""
Script for scheduling resources (routers, networks etc.) across gateway nodes.
"""

import openstack
from collections import Counter, defaultdict
from typing import Any, Iterator, Set, Dict, List, Optional
import random
import logging
import time
import sys
import argparse
from tabulate import tabulate


class ResourceNotFound(Exception):
    pass


class ErrorAddingReplica(Exception):
    pass


class ErrorRemovingReplica(Exception):
    pass


class ImpossibleRequirements(Exception):
    pass


class AgentNotFound(Exception):
    pass


def chunks(lst: List[Any], chunk_size: int) -> Iterator[List]:
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), chunk_size):
        yield lst[i : i + chunk_size]  # NOQA


""" Agent ID to Resource IDs mapping """
AgentResourceMapping = Dict[str, Set[str]]

""" Agent ID to Agent object mapping """
Agents = Dict[str, openstack.network.v2.agent.Agent]

""" Counter of Resource IDs and there Replica count """
Resources = Dict[str, int]


class BaseAgentHandler:
    def __init__(self, client) -> None:
        self.client = client

    def add_resource_to_agent(self, agent, resource):
        pass

    def remove_resource_from_agent(self, agent, resource):
        pass

    def get_all_agents(self):
        pass

    def find_resource(self, resource_id):
        pass

    def get_all_resources_hosted_by_agent(
        self, agent: openstack.network.v2.agent.Agent
    ):
        pass

    def _is_agent_useable(
        self, agent: openstack.network.v2.agent.Agent
    ) -> bool:
        pass


class DHCPAgentHandler(BaseAgentHandler):
    def __init__(
        self,
        client,
        dhcp_enabled,
        **kwargs,
    ) -> None:
        super().__init__(client)
        self.agent_type = "DHCP agent"
        self.dhcp_enabled = dhcp_enabled
        self.network_ids_with_enabled_dhcp: Set[str] = set()

    def get_all_agents(self):
        if self.dhcp_enabled:
            # Get a reference on networks where DHCP is enabled
            # in the subnet
            for subnet in self.client.network.subnets(is_dhcp_enabled=True):
                self.network_ids_with_enabled_dhcp.add(subnet.network_id)

        return self.client.network.agents(agent_type=self.agent_type)

    def add_resource_to_agent(
        self, agent: openstack.network.v2.agent.Agent, resource
    ):
        return self.client.network.add_dhcp_agent_to_network(agent, resource)

    def remove_resource_from_agent(self, agent, resource):
        return self.client.network.remove_dhcp_agent_from_network(
            agent, resource
        )

    def find_resource(self, resource_id):
        return self.client.network.find_network(resource_id)

    def _is_agent_useable(
        self, agent: openstack.network.v2.agent.Agent
    ) -> bool:
        return agent.is_alive and agent.is_admin_state_up

    def get_all_resources_hosted_by_agent(
        self, agent: openstack.network.v2.agent.Agent
    ):
        for network in self.client.network.dhcp_agent_hosting_networks(
            agent,
            is_admin_state_up=True,
        ):
            network_id = network["id"]
            if (
                not self.dhcp_enabled
                or network_id in self.network_ids_with_enabled_dhcp
            ):
                yield network


class L3AgentHandler(BaseAgentHandler):
    def __init__(self, client) -> None:
        self.agent_type = "L3 agent"
        super().__init__(client)

    def get_all_agents(self):
        return self.client.network.agents(
            agent_type=self.agent_type,
        )

    def add_resource_to_agent(
        self, agent: openstack.network.v2.agent.Agent, resource
    ):
        return self.client.network.add_router_to_agent(agent, resource)

    def remove_resource_from_agent(self, agent, resource):
        return self.client.network.remove_router_from_agent(agent, resource)

    def find_resource(self, resource_id):
        return self.client.network.find_router(resource_id)

    def get_all_resources_hosted_by_agent(
        self, agent: openstack.network.v2.agent.Agent
    ):
        return self.client.network.agent_hosted_routers(agent, is_ha=True)

    def _is_agent_useable(
        self, agent: openstack.network.v2.agent.Agent
    ) -> bool:
        return agent.is_alive and agent.is_admin_state_up

    def get_active_agent_for_resource(
        self, resource
    ) -> Optional[openstack.network.v2.agent.Agent]:
        """
        Returns the active router if there is one otherwise None
        """
        for agent in self.client.network.routers_hosting_l3_agents(resource):
            if agent.ha_state == "active":
                return agent

        return None


class AgentScheduler:
    def __init__(
        self,
        client: openstack.connection.Connection,
        agent_type: str,
        dry_run=False,
        logger=None,
        max_resources_per_agent=None,
        **kwargs,
    ) -> None:
        self.__agents: Agents = dict()
        self.__resources_on_agent: AgentResourceMapping = defaultdict(set)
        self.__resources: Resources = Counter()
        handler: BaseAgentHandler
        if agent_type == "dhcp":
            handler = DHCPAgentHandler(client=client, **kwargs)
        elif agent_type == "l3":
            handler = L3AgentHandler(client=client)
        self.__agent_handler = handler
        self.__dry_run = dry_run
        self.__logger = logger
        self.__max_resources_per_agent = max_resources_per_agent

    def _get_agent_name(self, agent_id: str) -> str:
        return self.__agents[agent_id].host

    def _get_all_agents(self) -> Iterator[openstack.network.v2.agent.Agent]:
        return self.__agent_handler.get_all_agents()

    def _is_agent_useable(
        self, agent: openstack.network.v2.agent.Agent
    ) -> bool:
        return self.__agent_handler._is_agent_useable(agent)

    def _get_all_resources_hosted_by_agent(
        self, agent: openstack.network.v2.agent.Agent
    ) -> Iterator[openstack.resource.Resource]:
        return self.__agent_handler.get_all_resources_hosted_by_agent(agent)

    def _fetch_agents_and_resources(self) -> None:
        for agent in self._get_all_agents():
            self.__agents[agent.id] = agent

            for resource in self._get_all_resources_hosted_by_agent(agent):
                resource_id = resource["id"]
                self.__resources_on_agent[agent.id].add(resource_id)
                self.__resources[resource_id] += 1

    def _get_agents_hosting_resource(
        self, resource_id: str
    ) -> List[openstack.network.v2.agent.Agent]:
        return [
            self.__agents[agent_id]
            for agent_id, resource_set in self.__resources_on_agent.items()
            if resource_id in resource_set
        ]

    def _get_available_agents_for_resource(
        self,
        resource_id: str,
        agents: List[openstack.network.v2.agent.Agent],
        excluded_azs: Set[str] = None,
    ) -> List[openstack.network.v2.agent.Agent]:
        """
        Returns a sorted list of agents to keep the agents balanced
        """
        possible_agents = []
        for agent in agents:
            if not self._is_agent_useable(agent):
                continue

            # exclude if agent is inside the excluded
            # Availability Zone set
            if excluded_azs and agent.availability_zone in excluded_azs:
                continue

            resources_on_this_agent = self.__resources_on_agent[agent.id]

            # is it already on this agent?
            # if yes this agent is not an option
            if resource_id in resources_on_this_agent:
                continue

            total_resources_on_this_agent = len(resources_on_this_agent)

            # Agent already has too many resources?
            if (
                self.__max_resources_per_agent
                and total_resources_on_this_agent
                >= self.__max_resources_per_agent
            ):
                continue

            possible_agents.append((agent, total_resources_on_this_agent))

        # Sorted in ascending order based on the number of resources per agent
        return [
            agent
            for agent, _ in sorted(possible_agents, key=lambda tup: tup[1])
        ]

    def _calc_fairshare(self, replicas: int) -> int:
        return len(self.__resources) * replicas // len(self.__agents)

    def schedule_replicas(
        self,
        replicas: int,
        batch_size: int = None,
        batch_wait_time: float = None,
        **kwargs,
    ):
        self._fetch_agents_and_resources()
        fairshare = self._calc_fairshare(replicas)
        if (
            self.__max_resources_per_agent
            and fairshare > self.__max_resources_per_agent
        ):
            raise ImpossibleRequirements(
                f"Impossible Fairshare to fulfil. Fairshare {fairshare} "
                + "is higher than the maximum of "
                + f"{self.__max_resources_per_agent} Resources per Agent"
            )

        resources_to_add_replicas = [
            resource_id
            for resource_id, current_replicas in self.__resources.items()
            if current_replicas < replicas
        ]

        random.shuffle(resources_to_add_replicas)

        if batch_size:
            for resource_chunk in chunks(
                resources_to_add_replicas, batch_size
            ):
                for resource_id in resource_chunk:
                    self._add_resource_to_agents(
                        resource_id=resource_id, wanted_replicas=replicas
                    )

                time.sleep(batch_wait_time or 1.0)
        else:
            for resource_id in resources_to_add_replicas:
                self._add_resource_to_agents(
                    resource_id=resource_id, wanted_replicas=replicas
                )

    def _add_resource_to_agents(
        self, resource_id: str, wanted_replicas: int
    ) -> None:
        resource = self.__agent_handler.find_resource(resource_id=resource_id)
        if not resource:
            raise ResourceNotFound(f"Unable to find Resource {resource_id}")

        agents_hosting_this_resource = self._get_agents_hosting_resource(
            resource_id=resource_id
        )
        current_replicas = self.__resources[resource_id]

        az_to_exclude = set(
            [agent.availability_zone for agent in agents_hosting_this_resource]
        )

        agents = self._get_available_agents_for_resource(
            resource_id=resource_id,
            agents=list(self.__agents.values()),
            excluded_azs=az_to_exclude,
        )

        replicas_to_add = wanted_replicas - current_replicas
        for _ in range(replicas_to_add):
            agent = agents.pop(0)
            self._add_resource_to_agent(agent, resource)

    def _add_resource_to_agent(self, agent, resource):
        resource_id = resource["id"]
        if not self.__dry_run:
            err = self.__agent_handler.add_resource_to_agent(agent, resource)
            if err:
                raise ErrorAddingReplica(err)

        self.__resources[resource_id] += 1
        self.__resources_on_agent[agent.id].add(resource_id)

    def _remove_resource_from_agent(self, agent, resource):
        resource_id = resource["id"]
        if not self.__dry_run:
            err = self.__agent_handler.remove_resource_from_agent(
                agent, resource
            )
            if err:
                raise ErrorRemovingReplica(err)

        self.__resources[resource_id] -= 1
        self.__resources_on_agent[agent.id].remove(resource_id)

    def drain_agents(
        self,
        agent_ids: List[str],
        excluded_agents: Set[str],
        filter_on_resource: str = None,
        verify_active: bool = None,
        ignore_azs: bool = None,
        replicas: int = None,
        **kwargs,
    ):
        self._fetch_agents_and_resources()

        for agents_to_drain in agent_ids:
            if agents_to_drain not in self.__agents:
                raise AgentNotFound(f"Agent {agents_to_drain} does not exist!")

        if excluded_agents:
            for excluded_agent in excluded_agents:
                if excluded_agent not in self.__agents:
                    raise AgentNotFound(
                        f"Excluded Agent {excluded_agent} does not exist!"
                    )

        available_agents = []
        for agent_id, agent in self.__agents.items():
            if agent_id in agent_ids:
                continue

            if excluded_agents and agent_id in excluded_agents:
                continue

            available_agents.append(agent)

        # check if the agent_handler supports get_active_agent_for_resource()
        get_active_agent_for_resource = getattr(
            self.__agent_handler, "get_active_agent_for_resource", None
        )

        for agent_id in agent_ids:
            agent_to_drain = self.__agents[agent_id]
            resources_on_this_agent = list(
                self.__resources_on_agent[agent_to_drain.id]
            )
            for idx, resource_id in enumerate(resources_on_this_agent):
                if filter_on_resource and resource_id != filter_on_resource:
                    continue
                self.__logger.info(f"{'*' * 20} {resource_id} {'*' * 20}")
                self.__logger.info(
                    f"Draining Agent {agent_id} ({agent_to_drain.host})"
                    " Processing Resource"
                    f" {idx + 1}/{len(resources_on_this_agent)}"
                )

                log_prefix = f"Resource {resource_id}:"
                resource = self.__agent_handler.find_resource(
                    resource_id=resource_id
                )
                if not resource:
                    raise ResourceNotFound(
                        f"Unable to find Resource {resource_id}"
                    )

                agents_hosting_this_resource = (
                    self._get_agents_hosting_resource(resource_id=resource_id)
                )
                current_replicas = len(agents_hosting_this_resource)
                agents_hosting_this_resource = [
                    agent
                    for agent in agents_hosting_this_resource
                    if agent != agent_to_drain
                ]

                if ignore_azs:
                    az_to_exclude = set()
                else:
                    az_to_exclude = set(
                        [
                            agent.availability_zone
                            for agent in agents_hosting_this_resource
                        ]
                    )

                agents = self._get_available_agents_for_resource(
                    resource_id=resource_id,
                    agents=available_agents,
                    excluded_azs=az_to_exclude,
                )
                if not agents and not (
                    replicas is None or
                    replicas <= current_replicas
                ):
                    self.__logger.error(f"{log_prefix} No agents available")
                    continue

                try:
                    if replicas is None or replicas >= current_replicas:
                        # if this is the case, the wanted replicas are already
                        # fulfilled
                        agent = agents.pop(0)
                        self.__logger.info(
                            f"{log_prefix} Adding to Agent"
                            f" {agent.id} ({agent.host})"
                        )

                        self._add_resource_to_agent(agent, resource)
                        self.__logger.info(
                            f"{log_prefix} Added to Agent"
                            f" {agent.id} ({agent.host})"
                        )

                        # None is not callable, so no need to check
                        if callable(get_active_agent_for_resource):
                            active_agent = get_active_agent_for_resource(
                                resource
                            )
                            if active_agent.id == agent_id:
                                self.__logger.info(
                                    f"{log_prefix} Active on"
                                    f" {active_agent.id} "
                                    f"({active_agent.host}),"
                                    " wait until everything calms down"
                                )
                                time.sleep(10)
                    elif replicas is not None:
                        self.__logger.info(
                            f"{log_prefix} The router already has enough"
                            " replicas"
                        )

                    # remove from drained agent
                    agent = self.__agents[agent_id]
                    self.__logger.info(
                        f"{log_prefix} Removing from Agent"
                        f" {agent.id} ({agent.host})"
                    )
                    self._remove_resource_from_agent(agent, resource)
                    self.__logger.info(
                        f"{log_prefix} Removed from Agent"
                        f" {agent.id} ({agent.host})"
                    )

                    if (
                        callable(get_active_agent_for_resource)
                        and verify_active
                    ):
                        time.sleep(3)
                        wait_time_s = 30
                        active_agent = None
                        for _ in range(wait_time_s):
                            active_agent = get_active_agent_for_resource(
                                resource
                            )
                            if active_agent:
                                break
                            time.sleep(1)
                        if active_agent:
                            self.__logger.info(
                                f"{log_prefix} Now Active on agent"
                                f" {active_agent.id} ({active_agent.host})"
                            )
                        else:
                            self.__logger.error(
                                f"{log_prefix} not active anywhere"
                                f" after {wait_time_s} seconds."
                            )
                    else:
                        time.sleep(1)

                    self.__logger.info(f"Finished resource {resource_id}")
                except (ErrorAddingReplica, ErrorRemovingReplica) as err:
                    self.__logger.crit(f"{log_prefix} Exception: {err}")

    def force_replicas(self, replicas: int, **kwargs):
        self._fetch_agents_and_resources()

        for resource_id, resource_replicas in self.__resources.items():
            if resource_replicas > replicas:
                # delete
                resource = self.__agent_handler.find_resource(
                    resource_id=resource_id
                )
                if not resource:
                    raise ResourceNotFound(
                        f"Unable to find Resource {resource_id}"
                    )

                agents_hosting_this_resource = (
                    self._get_agents_hosting_resource(resource_id=resource_id)
                )
                active_agent = (
                    self.__agent_handler.get_active_agent_for_resource(
                        resource=resource_id
                    )
                )
                counter = resource_replicas
                while counter > replicas:
                    agent = agents_hosting_this_resource.pop(0)
                    if active_agent.id == agent.id:
                        self.__logger.info(
                            f"Resource {resource_id}: Skipping"
                            f" {agent.id} ({agent.host}) as it is Active"
                        )
                        continue
                    self._remove_resource_from_agent(agent, resource)
                    counter -= 1
                    self.__logger.info(
                        f"Resource {resource_id}: Removing from agent"
                        f" {agent.id} ({agent.host}) | Now has"
                        f" {counter} replicas"
                    )

    def count_resources_per_agent(self, **kwargs):
        self._fetch_agents_and_resources()

        headers = [
            "ID",
            "Agent",
            "Availability Zone",
            "Resources",
            "Up",
            "Alive",
        ]
        table = []
        for agent_id, agent in self.__agents.items():
            resources = self.__resources_on_agent[agent_id]
            table.append(
                (
                    agent.id,
                    agent.host,
                    agent.availability_zone,
                    len(resources),
                    agent.is_admin_state_up,
                    agent.is_alive,
                )
            )

        print(tabulate(table, headers))


def create_file_handler(
    filename: str, loglevel: int, fmt: logging.Formatter = None
) -> logging.FileHandler:
    fh = logging.FileHandler(filename)
    fh.setLevel(loglevel)
    if fmt:
        fh.setFormatter(fmt)
    return fh


def create_stream_handler(
    loglevel: int, fmt: logging.Formatter = None
) -> logging.StreamHandler:
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    if fmt:
        ch.setFormatter(fmt)
    return ch


def setup_logger(args):
    # initialize logger
    loglevel = {
        0: logging.ERROR,
        1: logging.INFO,
        2: logging.DEBUG,
    }.get(args.verbose, logging.INFO)
    log_format = "[%(asctime)s] %(levelname)s %(message)s"
    logger = logging.getLogger("AgentScheduler")
    logger.setLevel(loglevel)
    formatter = logging.Formatter(log_format)
    ct = create_stream_handler(loglevel=loglevel, fmt=formatter)
    logger.addHandler(ct)
    if args.logfile:
        fh = create_file_handler(
            filename=args.logfile, loglevel=loglevel, fmt=formatter
        )
        logger.addHandler(fh)

    return logger


def setup_argparser():
    parser = argparse.ArgumentParser(
        description="Functionality around the Neutron Agents"
    )
    parser.add_argument(
        "--verbose",
        "-v",
        action="count",
        default=0,
        help="Increase verbosity (up to -vv)",
    )
    parser.add_argument(
        "--agent", required=True, choices=("l3", "dhcp"), dest="agent_type"
    )

    subparser = parser.add_subparsers(help="action")
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Do a dry run without modifying anything on remote",
    )
    parser.add_argument("-l", "--logfile", help="File to log to", default=None)
    drain_parser = subparser.add_parser(
        "drain", help="Drain one or more agents"
    )
    drain_parser.add_argument(
        "-d",
        "--agents-to-drain",
        nargs="+",
        default=None,
        help="agent ids to drain",
        required=True,
        dest="agent_ids",
    )
    drain_parser.add_argument(
        "-e",
        "--excluded-agents",
        nargs="+",
        default=None,
        help="agent ids to which no resource may be moved",
        required=False,
    )
    drain_parser.add_argument(
        "-m",
        "--max-resources-per-agent",
        type=int,
        default=None,
        help="hard limit for for resources per agent",
        required=False,
    )
    drain_parser.add_argument(
        "--verify-active",
        action="store_true",
        help=(
            "Verifies that the router has the status active on one of its"
            " agents after the migration of the router."
        ),
    )
    drain_parser.add_argument(
        "-f",
        "--filter-resource",
        type=str,
        default=None,
        dest="filter_on_resource",
        help="Only migrate this resource id for testing purposes",
    )
    drain_parser.add_argument(
        "--ignore-azs",
        action="store_true",
        help=(
            "Allow the resources to be schedulded to the same Availability"
            " Zone"
        ),
    )
    drain_parser.add_argument(
        "--dhcp-enabled",
        help="Consider only networks where the subnet has DHCP enabled.",
        action="store_true",
    )
    drain_parser.add_argument(
        "--replicas",
        type=int,
        help="amount of replicas per resource",
        default=None,
    )
    drain_parser.set_defaults(func="drain")

    add_replicas_parser = subparser.add_parser(
        "add-replicas", help="Balance the replicas between Agents"
    )
    add_replicas_parser.add_argument(
        "--replicas",
        type=int,
        default=None,
        help="amount of replicas per resources",
        required=True,
    )
    add_replicas_parser.add_argument(
        "-m",
        "--max-resources-per-agent",
        type=int,
        default=None,
        help="hard limit for for resources per agent",
        required=False,
    )
    add_replicas_parser.add_argument(
        "--wait",
        type=int,
        default=None,
        help="Time to wait between add of resources in seconds",
    )
    add_replicas_parser.add_argument(
        "--batch-size",
        type=int,
        default=None,
        help="Process resources in chunks, this is the chunk size",
    )
    add_replicas_parser.add_argument(
        "--batch-wait-time",
        type=float,
        default=None,
        help="Time to wait between chunks in seconds",
    )
    add_replicas_parser.add_argument(
        "--dhcp-enabled",
        help="Consider only networks where the subnet has DHCP enabled.",
        action="store_true",
    )
    add_replicas_parser.set_defaults(func="add-replicas")

    force_replicas_parser = subparser.add_parser("force-replicas", help="")
    force_replicas_parser.add_argument(
        "--replicas",
        type=int,
        help="amount of replicas per resource",
        required=True,
    )
    force_replicas_parser.add_argument(
        "--dhcp-enabled",
        help="Consider only networks where the subnet has DHCP enabled.",
        action="store_true",
    )
    force_replicas_parser.set_defaults(func="force-replicas")

    count_resources_per_agent_parser = subparser.add_parser(
        "count-resources-per-agent", help=""
    )
    count_resources_per_agent_parser.add_argument(
        "--dhcp-enabled",
        help="Consider only networks where the subnet has DHCP enabled.",
        action="store_true",
    )
    count_resources_per_agent_parser.set_defaults(
        func="count-resources-per-agent"
    )

    return parser


def main() -> int:
    parser = setup_argparser()
    args = parser.parse_args()
    kwargs = vars(args)

    logger = setup_logger(args)
    kwargs["logger"] = logger

    os_client = openstack.connect()
    scheduler = AgentScheduler(os_client, **kwargs)
    if args.func == "drain":
        scheduler.drain_agents(**kwargs)
    elif args.func == "add-replicas":
        scheduler.schedule_replicas(**kwargs)
    elif args.func == "force-replicas":
        scheduler.force_replicas(**kwargs)
    elif args.func == "count-resources-per-agent":
        scheduler.count_resources_per_agent(**kwargs)
    else:
        return -1
    return 0


if __name__ == "__main__":
    sys.exit(main())
