#!/usr/bin/env python3
from kubernetes import client, config
import os

NEW_IMAGE = os.environ["NEW_IMAGE"]


def patch_requires_recreation(api, cr_object):
    api.patch_namespaced_custom_object_status(
        group="compute.yaook.cloud",
        version="v1",
        namespace="yaook",
        plural="novacomputenodes",
        name=cr_object["metadata"]["name"],
        body=cr_object,
    )


def get_compute_sts(apps_client, hostname):
    return apps_client.list_namespaced_stateful_set(
        namespace="yaook",
        label_selector=f"state.yaook.cloud/component=compute,state.yaook.cloud/parent-group=compute.yaook.cloud,state.yaook.cloud/parent-name={hostname}",  # noqa: E501
    ).items[0]


def patch_compute_sts(apps_client, sts_name, resource):
    apps_client.patch_namespaced_stateful_set(
        name=sts_name, namespace="yaook", body=resource
    )


def get_compute_correct_secret(v1_api, hostname):
    return (
        v1_api.list_namespaced_secret(
            namespace="yaook",
            label_selector=f"state.yaook.cloud/parent-name={hostname},!state.yaook.cloud/orphaned,state.yaook.cloud/component=compute_config",  # noqa: E501
        )
        .items[0]
        .metadata.name
    )


def main():
    # desired context of you kubeconf
    KUBECONF = os.environ["KUBECONF_CONTEXT"]
    config.load_kube_config(context=KUBECONF)
    api = client.CustomObjectsApi()
    v1_api = client.CoreV1Api()
    apps_client = client.AppsV1Api()
    resource = api.list_namespaced_custom_object(
        group="compute.yaook.cloud",
        version="v1",
        namespace="yaook",
        plural="novacomputenodes",
    )
    for cr in resource["items"]:
        print(cr["metadata"]["name"])
        # check requires recreation
        if cr["status"] and cr["status"]["conditions"]:
            sts = get_compute_sts(
                apps_client, hostname=cr["metadata"]["name"]
            )
            for container in sts.spec.template.spec.containers:
                container.image = NEW_IMAGE
            for init_container in sts.spec.template.spec.init_containers:
                init_container.image = NEW_IMAGE
            for volume in sts.spec.template.spec.volumes:
                if volume.name == "nova-config-volume":
                    volume.secret.secret_name = get_compute_correct_secret(
                        v1_api=v1_api, hostname=cr["metadata"]["name"]
                    )
            patch_compute_sts(apps_client, sts.metadata.name, sts)
            for i in range(len(cr["status"]["conditions"])):
                if (
                    cr["status"]["conditions"][i]["type"]
                    == "RequiresRecreation"
                ):
                    # patch node
                    del cr["status"]["conditions"][i]
                    if cr["status"].get("eviction", None):
                        cr["status"]["eviction"] == "null"
                    if cr["status"].get("phase") != "Updated":
                        cr["status"]["phase"] = "Updated"
                    if cr["status"].get("state") != "Enabled":
                        cr["status"]["state"] = "Enabled"
                    patch_requires_recreation(api, cr)
                    break


if __name__ == "__main__":
    main()
