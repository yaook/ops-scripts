# os ovs check gateway broadcast flows

run this on a compute node to validate all broadcast flows.
It needs access to a neutron/nova config file to get credentials from.

Run it using `./os-ovs-cheeck-gateway-broadcast-flows --os-config <path-to-nova/neutron-config>`

It can generate metrics for prometheus.

Note: this script was originally not writting with yaook in mind, so you might need to do some adjustments.
