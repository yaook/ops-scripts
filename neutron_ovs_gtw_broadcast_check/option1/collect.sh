#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage $0 <jumphost>"
    exit 1
fi


function get_ns {
    ssh -J "$1" "$2" -o stricthostkeychecking=no < _get_ns.sh | grep namespace > "$2_ns"
}

function vsctl {
    ssh -J "$1" "$2" -o stricthostkeychecking=no "sudo ovs-vsctl show" > "$2_vsctl"
}

function flows {
    ssh -J "$1" "$2" -o stricthostkeychecking=no "sudo ovs-ofctl dump-flows br-tun" > "$2_flows"
}

function iface-ids {
    ssh -J "$1" "$2" -o stricthostkeychecking=no "sudo ovs-ofctl show br-tun" > "$2_ifaceids"
}

rm -rf gtw*

get_ns "$1" gtw01 &
get_ns "$1" gtw02 &
get_ns "$1" gtw03 &

wait

vsctl "$1" gtw01 &
vsctl "$1" gtw02 &
vsctl "$1" gtw03 &
flows "$1" gtw01 &
flows "$1" gtw02 &
flows "$1" gtw03 &
iface-ids "$1" gtw01 &
iface-ids "$1" gtw02 &
iface-ids "$1" gtw03 &

wait
