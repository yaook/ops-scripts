# OVS GTW Broadcast check

this allows you to validate on your compute nodes that all broadcast flows to your gateway/network nodes are  existing and valid.

## Usage

in `collect.sh` and `validate.py` update the your network nodes (currently named as `gtw01` to `gtw03`).
Then you can run `collect.sh` and `validate.py` to see if anything is off.

In case a missmatch is found it also shows you the commands to inject the broadcast flows.
Note: the neutron-openvswitch-agent might want to remove your newly added flows again. So you might need to run this in a while true loop.


