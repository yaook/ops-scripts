#!/bin/bash
sudo su <<'EOF'
for i in $(ip netns |grep qrouter | awk '{print $1}'); do echo -n "namespace;$(echo $i | cut -d \- -f 2-);"; ip netns exec $i ip l | grep 'qr-' | awk '{print $2}' | cut -d : -f 1| head -1; echo ""; done
EOF
