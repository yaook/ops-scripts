#!/usr/bin/env python3
import re


IGNORE_IPS = set(
    [
        "172.25.1.131",
        "172.25.1.132",
        "172.25.1.133",
        "172.25.249.131",
        "172.25.249.132",
        "172.25.249.133",
    ]
)
NODES = ["gtw01", "gtw02", "gtw03"]


def _reverse_dict(d):
    n = {}
    for k, v in d.items():
        n[v] = k
    return n


def get_vlan(node):
    ifaces = {}
    with open("%s_ns" % node) as namespaces:
        for line in namespaces:
            p = line.split(";")
            if p[2].strip() != "":
                ifaces[p[1]] = p[2].strip()
    vlans = {}
    with open("%s_vsctl" % node) as vsctl:
        port = None
        for line in vsctl:
            if line.startswith("        Port "):
                port = (
                    line.replace("        Port ", "").replace('"', "").strip()
                )
            if line.startswith("            tag: "):
                vlans[port] = line.split(":")[1].strip()
                port = None
    mapping = {}
    for n, i in ifaces.items():
        mapping[n] = vlans[i]
    return mapping


def get_flows(node):
    outputs = {}
    with open("%s_flows" % node) as f:
        for line in f:
            if "table=22" in line and "dl_vlan=" in line:
                p = line.split("dl_vlan=")[1].split(" ")
                vlan = p[0]
                curr_outputs = []
                for k in p[1].split(","):
                    if k.startswith("output:"):
                        curr_outputs.append(k.split(":")[1].strip())
                outputs[vlan] = (curr_outputs, line.strip())
    return outputs


def get_remote_ips(node):
    iface_ids = {}
    with open("%s_ifaceids" % node) as f:
        for line in f:
            if "vxlan-" in line:
                p = line.strip().split(")")[0].split("(")
                iface_ids[p[0]] = p[1]
    remote_ips = {}
    with open("%s_vsctl" % node) as vsctl:
        port = None
        for line in vsctl:
            if line.startswith("        Port "):
                port = (
                    line.replace("        Port ", "").replace('"', "").strip()
                )
            if (
                line.startswith("                options: ")
                and "remote_ip=" in line
            ):
                ip = line.split("remote_ip=")[1].split('"')[1]
                remote_ips[port] = ip
                port = None
    output = {}
    for id, name in iface_ids.items():
        output[id] = remote_ips[name]
    return output


def get_outputs(node):
    vlans = get_vlan(node)
    flows = get_flows(node)
    remote_ips = get_remote_ips(node)
    ns_outputs = {}
    mappings = {}
    lines = {}
    for ns, vl in vlans.items():
        try:
            mappings[ns] = flows[vl][0]
            lines[ns] = flows[vl][1]
            ns_outputs[ns] = flows[vl][0]
        except KeyError:
            mappings[ns] = []
            lines[ns] = []
    for k in mappings:
        ips = []
        for iface in mappings[k]:
            try:
                ips.append(remote_ips[iface])
            except KeyError:
                print(
                    "ERROR: node: %s namespace: %s interface_id: %s "
                    "HAS NOT MATCHING INTERFACE" % (node, k, iface)
                )
        mappings[k] = ips
    return mappings, lines, ns_outputs, remote_ips


def get_output_diffs(namespace, mapping):
    all_ips = set()
    for node in NODES:
        all_ips = all_ips.union(set(mapping[node][namespace]))
    all_ips = all_ips.difference(IGNORE_IPS)

    err = False
    node_errs = {}
    for node in NODES:
        diff = all_ips.difference(mapping[node][namespace])
        node_errs[node] = diff
        if len(diff) > 0:
            err = True
    return err, all_ips, node_errs


def generate_fix(all_ips, line, ns_outputs, remote_ips):
    ip_map = _reverse_dict(remote_ips)
    out = re.sub(
        r", (duration|n_packets|n_bytes|idle_age|hard_age)=" r"([0-9\.]+)s?",
        "",
        line,
    )
    out = re.sub(r",output:.*$", "", out)
    out = re.sub(r" actions=", ",actions=", out)
    out = out.replace(" ", "")
    output_ids = set()
    for ip in all_ips:
        output_ids.add(ip_map[ip])
    for out_id in ns_outputs:
        output_ids.add(out_id)
    for out_id in sorted(output_ids):
        out = out + ",output:%s" % out_id
    return "ovs-ofctl add-flow br-tun '%s'" % out


if __name__ == "__main__":
    mapping = {}
    lines = {}
    ns_outputs = {}
    remote_ips = {}
    global_ns = set()
    for node in NODES:
        (
            mapping[node],
            lines[node],
            ns_outputs[node],
            remote_ips[node],
        ) = get_outputs(node)
        global_ns = global_ns | mapping[node].keys()

    fixes = {}
    for namespace in global_ns:
        err, all_ips, node_errs = get_output_diffs(namespace, mapping)
        if err:
            print("Something bad in %s; %s" % (namespace, node_errs))
            for node in NODES:
                if len(node_errs[node]) != 0:
                    fix = generate_fix(
                        all_ips,
                        lines[node][namespace],
                        ns_outputs[node][namespace],
                        remote_ips[node],
                    )
                    fix = fix + " #%s" % namespace
                    if node not in fixes:
                        fixes[node] = set()
                    fixes[node].add(fix)

    if len(fixes) > 0:
        print("\n\nFIXES:")
        for node in sorted(fixes.keys()):
            print("\n%s" % node)
            for fix in fixes[node]:
                print(fix)
    else:
        print("All is fine")
