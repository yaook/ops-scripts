import validate
import unittest


class TestValidations(unittest.TestCase):
    def test_extract_vlans(self):
        vlans = validate.get_vlan("test_assets/test_extract_vlans")
        self.assertEqual(vlans["946f3f8b-581a-4206-806a-5990f8751902"], "1346")
        self.assertEqual(vlans["0920fe32-9d68-40e5-a32d-343159178e13"], "1342")

    def test_get_flows(self):
        flows = validate.get_flows("test_assets/test_get_flows")
        self.assertEqual(flows["12"][0], ["3", "4", "5", "15"])
        self.assertEqual(
            flows["12"][1],
            "cookie=0x934fdd90974604e8, duration=256928.662s, table=22,"
            " n_packets=220, n_bytes=15256, idle_age=267, hard_age=65534,"
            " priority=1,dl_vlan=12"
            " actions=strip_vlan,load:0x32->NXM_NX_TUN_ID[],output:3,output:4,output:5,output:15",  # NOQA: E501
        )  # noqa: E501
        self.assertEqual(flows["25"][0], ["15", "130", "5"])
        self.assertEqual(
            flows["25"][1],
            "cookie=0x934fdd90974604e8, duration=150442.646s, table=22,"
            " n_packets=75222, n_bytes=4062628, idle_age=1, hard_age=65534,"
            " priority=1,dl_vlan=25"
            " actions=strip_vlan,load:0x24->NXM_NX_TUN_ID[],output:15,output:130,output:5",  # NOQA: E501
        )  # noqa: E501

    def test_get_remote_ips(self):
        ips = validate.get_remote_ips("test_assets/test_get_remote_ips")
        self.assertEqual(ips["15"], "172.25.249.131")
        self.assertEqual(ips["130"], "172.25.249.137")

    def test_get_outputs(self):
        mapping, lines, ns_outputs, remote_ips = validate.get_outputs(
            "test_assets/test_get_outputs"
        )
        self.assertEqual(
            mapping["606c03f9-604e-4f35-91b8-39d5c3f06748"],
            [
                "172.25.249.135",
                "172.25.249.136",
                "172.25.249.133",
                "172.25.249.131",
            ],
        )  # noqa: E501
        self.assertEqual(
            mapping["28b3b705-ecaf-4d80-96d4-a399524e4999"],
            ["172.25.249.133", "172.25.249.131"],
        )  # noqa: E501
        self.assertEqual(
            lines["606c03f9-604e-4f35-91b8-39d5c3f06748"],
            "cookie=0x934fdd90974604e8, duration=257202.263s, table=22,"
            " n_packets=221, n_bytes=15326, idle_age=148, hard_age=65534,"
            " priority=1,dl_vlan=12"
            " actions=strip_vlan,load:0x32->NXM_NX_TUN_ID[],output:3,output:4,output:5,output:15",  # NOQA: E501
        )  # noqa: E501
        self.assertEqual(
            lines["28b3b705-ecaf-4d80-96d4-a399524e4999"],
            "cookie=0x934fdd90974604e8, duration=257203.036s, table=22,"
            " n_packets=110, n_bytes=7492, idle_age=279, hard_age=65534,"
            " priority=1,dl_vlan=1241"
            " actions=strip_vlan,load:0x64->NXM_NX_TUN_ID[],output:5,output:15",  # NOQA: E501
        )  # noqa: E501
        self.assertEqual(
            ns_outputs["606c03f9-604e-4f35-91b8-39d5c3f06748"],
            ["3", "4", "5", "15"],
        )  # noqa: E501
        self.assertEqual(
            ns_outputs["28b3b705-ecaf-4d80-96d4-a399524e4999"], ["5", "15"]
        )  # noqa: E501
        self.assertEqual(remote_ips["3"], "172.25.249.135")
        self.assertEqual(remote_ips["4"], "172.25.249.136")
        self.assertEqual(remote_ips["5"], "172.25.249.133")
        self.assertEqual(remote_ips["15"], "172.25.249.131")

    def test_generate_fix(self):
        all_ips = set(["172.25.249.135", "172.25.249.133", "172.25.249.131"])
        line = (  # NOQA: E501
            "cookie=0x934fdd90974604e8, duration=257203.036s, table=22,"
            " n_packets=110, n_bytes=7492, idle_age=279, hard_age=65534,"
            " priority=1,dl_vlan=1241"
            " actions=strip_vlan,load:0x64->NXM_NX_TUN_ID[],output:5,output:15"
        )
        ns_outputs = ["5", "15"]
        remote_ips = {
            "3": "172.25.249.135",
            "4": "172.25.249.136",
            "5": "172.25.249.133",
            "15": "172.25.249.131",
        }
        newline = validate.generate_fix(all_ips, line, ns_outputs, remote_ips)
        self.assertEqual(
            newline,
            "ovs-ofctl add-flow br-tun"
            " 'cookie=0x934fdd90974604e8,table=22,priority=1,dl_vlan=1241,actions=strip_vlan,load:0x64->NXM_NX_TUN_ID[],output:15,output:3,output:5'",  # NOQA: E501
        )  # noqa: E501
