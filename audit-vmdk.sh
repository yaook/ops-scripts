#!/bin/bash
set -euo pipefail
# Audit a ceph RBD pool for images (i.e. OpenStack images or volumes) which
# start with a VMDK header.
# This may be used to find objects which are being used to exploit
# CVE-2022-47951.

myname="$(basename "$0")"
if [ "$#" != 1 ]; then
    printf 'usage: %s POOLNAME\n' "$myname" >&2
    exit 1
fi

CEPH_ARGS=''
IMAGE_POOL="$1"
workdir="$(mktemp -d)"
function cleanup() {
    rm -rf -- "$workdir"
}
trap cleanup EXIT

for image in $(rbd $CEPH_ARGS --pool images ls); do
    block_prefix="$(rbd --pool images info "$image" | grep -Po '(?<=block_name_prefix: )\S+')"
    if ! rados $CEPH_ARGS --pool images get "$block_prefix".0000000000000000 "$workdir/block"; then
        printf 'failed to read first block of %s\n' "$image"
        continue
    fi
    head -c22 "$workdir/block" | grep -q 'DescriptorFile' && printf '%s\n' "$image"
done
